-- MySQL dump 10.11
--
-- Host: localhost    Database: caribe
-- ------------------------------------------------------
-- Server version	5.0.91-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app_plugins_plugin`
--

DROP TABLE IF EXISTS `app_plugins_plugin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_plugins_plugin` (
  `id` int(11) NOT NULL,
  `point_id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `index` int(11) NOT NULL,
  `registered` tinyint(1) NOT NULL,
  `status` smallint(6) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `template` text NOT NULL,
  `_order` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `label` (`label`),
  KEY `app_plugins_plugin_61a58e2f` (`point_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_plugins_pluginpoint`
--

DROP TABLE IF EXISTS `app_plugins_pluginpoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_plugins_pluginpoint` (
  `id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `index` int(11) NOT NULL,
  `registered` tinyint(1) NOT NULL,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `label` (`label`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_plugins_userpluginpreference`
--

DROP TABLE IF EXISTS `app_plugins_userpluginpreference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_plugins_userpluginpreference` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `plugin_id` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `index` int(11) NOT NULL,
  `_order` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_id` (`user_id`,`plugin_id`),
  KEY `app_plugins_userpluginpreference_403f60f` (`user_id`),
  KEY `app_plugins_userpluginpreference_2857ccbf` (`plugin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `area_taxrate`
--

DROP TABLE IF EXISTS `area_taxrate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area_taxrate` (
  `id` int(11) NOT NULL,
  `taxClass_id` int(11) NOT NULL,
  `taxZone_id` int(11) default NULL,
  `taxCountry_id` int(11) default NULL,
  `percentage` decimal(10,0) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `area_taxrate_21ec4f69` (`taxClass_id`),
  KEY `area_taxrate_1a7b2a7e` (`taxZone_id`),
  KEY `area_taxrate_15e55885` (`taxCountry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_425ae3c4` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_message`
--

DROP TABLE IF EXISTS `auth_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_message` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `auth_message_403f60f` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_403f60f` (`user_id`),
  KEY `auth_user_groups_425ae3c4` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_403f60f` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configurable_configurableproduct`
--

DROP TABLE IF EXISTS `configurable_configurableproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configurable_configurableproduct` (
  `product_id` int(11) NOT NULL,
  `create_subs` tinyint(1) NOT NULL,
  PRIMARY KEY  (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configurable_configurableproduct_option_group`
--

DROP TABLE IF EXISTS `configurable_configurableproduct_option_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configurable_configurableproduct_option_group` (
  `id` int(11) NOT NULL,
  `configurableproduct_id` int(11) NOT NULL,
  `optiongroup_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `configurableproduct_id` (`configurableproduct_id`,`optiongroup_id`),
  KEY `configurable_configurableproduct_option_group_21c384bb` (`configurableproduct_id`),
  KEY `configurable_configurableproduct_option_group_2d4ce28b` (`optiongroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configurable_productvariation`
--

DROP TABLE IF EXISTS `configurable_productvariation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configurable_productvariation` (
  `product_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY  (`product_id`),
  KEY `configurable_productvariation_63f17a16` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configurable_productvariation_options`
--

DROP TABLE IF EXISTS `configurable_productvariation_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configurable_productvariation_options` (
  `id` int(11) NOT NULL,
  `productvariation_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `productvariation_id` (`productvariation_id`,`option_id`),
  KEY `configurable_productvariation_options_7a469d48` (`productvariation_id`),
  KEY `configurable_productvariation_options_2f3b0dc9` (`option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact_addressbook`
--

DROP TABLE IF EXISTS `contact_addressbook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_addressbook` (
  `id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `description` varchar(20) NOT NULL,
  `addressee` varchar(80) NOT NULL,
  `street1` varchar(80) NOT NULL,
  `street2` varchar(80) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `postal_code` varchar(30) NOT NULL,
  `country_id` int(11) NOT NULL,
  `is_default_shipping` tinyint(1) NOT NULL,
  `is_default_billing` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `contact_addressbook_170b8823` (`contact_id`),
  KEY `contact_addressbook_534dd89` (`country_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact_contact`
--

DROP TABLE IF EXISTS `contact_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_contact` (
  `id` int(11) NOT NULL,
  `title` varchar(30) default NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `user_id` int(11) default NULL,
  `role_id` varchar(30) default NULL,
  `organization_id` int(11) default NULL,
  `dob` date default NULL,
  `email` varchar(75) NOT NULL,
  `notes` text NOT NULL,
  `create_date` date NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `contact_contact_40f80fc0` (`role_id`),
  KEY `contact_contact_68283273` (`organization_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact_contactinteractiontype`
--

DROP TABLE IF EXISTS `contact_contactinteractiontype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_contactinteractiontype` (
  `key` varchar(30) NOT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY  (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact_contactorganization`
--

DROP TABLE IF EXISTS `contact_contactorganization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_contactorganization` (
  `key` varchar(30) NOT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY  (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact_contactorganizationrole`
--

DROP TABLE IF EXISTS `contact_contactorganizationrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_contactorganizationrole` (
  `key` varchar(30) NOT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY  (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact_contactrole`
--

DROP TABLE IF EXISTS `contact_contactrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_contactrole` (
  `key` varchar(30) NOT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY  (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact_interaction`
--

DROP TABLE IF EXISTS `contact_interaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_interaction` (
  `id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `type_id` varchar(30) NOT NULL,
  `date_time` datetime NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `contact_interaction_170b8823` (`contact_id`),
  KEY `contact_interaction_777d41c8` (`type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact_organization`
--

DROP TABLE IF EXISTS `contact_organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_organization` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type_id` varchar(30) default NULL,
  `role_id` varchar(30) default NULL,
  `create_date` date NOT NULL,
  `notes` text,
  PRIMARY KEY  (`id`),
  KEY `contact_organization_777d41c8` (`type_id`),
  KEY `contact_organization_40f80fc0` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact_phonenumber`
--

DROP TABLE IF EXISTS `contact_phonenumber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_phonenumber` (
  `id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `contact_phonenumber_170b8823` (`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `custom_customproduct`
--

DROP TABLE IF EXISTS `custom_customproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_customproduct` (
  `product_id` int(11) NOT NULL,
  `downpayment` int(11) NOT NULL,
  `deferred_shipping` tinyint(1) NOT NULL,
  PRIMARY KEY  (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `custom_customproduct_option_group`
--

DROP TABLE IF EXISTS `custom_customproduct_option_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_customproduct_option_group` (
  `id` int(11) NOT NULL,
  `customproduct_id` int(11) NOT NULL,
  `optiongroup_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `customproduct_id` (`customproduct_id`,`optiongroup_id`),
  KEY `custom_customproduct_option_group_1603c4dd` (`customproduct_id`),
  KEY `custom_customproduct_option_group_2d4ce28b` (`optiongroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `custom_customtextfield`
--

DROP TABLE IF EXISTS `custom_customtextfield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_customtextfield` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `products_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `price_change` decimal(10,0) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `slug` (`slug`,`products_id`),
  KEY `custom_customtextfield_56ae2a2a` (`slug`),
  KEY `custom_customtextfield_4594d88` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `custom_customtextfieldtranslation`
--

DROP TABLE IF EXISTS `custom_customtextfieldtranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_customtextfieldtranslation` (
  `id` int(11) NOT NULL,
  `customtextfield_id` int(11) NOT NULL,
  `languagecode` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `version` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `customtextfield_id` (`customtextfield_id`,`languagecode`,`version`),
  KEY `custom_customtextfieldtranslation_6fda757f` (`customtextfield_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) default NULL,
  `object_id` text,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `django_admin_log_403f60f` (`user_id`),
  KEY `django_admin_log_1bb8f392` (`content_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_comment_flags`
--

DROP TABLE IF EXISTS `django_comment_flags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_comment_flags` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `flag_date` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_id` (`user_id`,`comment_id`,`flag`),
  KEY `django_comment_flags_403f60f` (`user_id`),
  KEY `django_comment_flags_64c238ac` (`comment_id`),
  KEY `django_comment_flags_111c90f9` (`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_comments`
--

DROP TABLE IF EXISTS `django_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_comments` (
  `id` int(11) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `object_pk` text NOT NULL,
  `site_id` int(11) NOT NULL,
  `user_id` int(11) default NULL,
  `user_name` varchar(50) NOT NULL,
  `user_email` varchar(75) NOT NULL,
  `user_url` varchar(200) NOT NULL,
  `comment` text NOT NULL,
  `submit_date` datetime NOT NULL,
  `ip_address` char(15) default NULL,
  `is_public` tinyint(1) NOT NULL,
  `is_removed` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `django_comments_1bb8f392` (`content_type_id`),
  KEY `django_comments_6223029` (`site_id`),
  KEY `django_comments_403f60f` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` text NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY  (`session_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `l10n_adminarea`
--

DROP TABLE IF EXISTS `l10n_adminarea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `l10n_adminarea` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `abbrev` varchar(3) default NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `l10n_adminarea_534dd89` (`country_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `l10n_country`
--

DROP TABLE IF EXISTS `l10n_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `l10n_country` (
  `id` int(11) NOT NULL,
  `iso2_code` varchar(2) NOT NULL,
  `name` varchar(128) NOT NULL,
  `printable_name` varchar(128) NOT NULL,
  `iso3_code` varchar(3) NOT NULL,
  `numcode` smallint(5) unsigned default NULL,
  `active` tinyint(1) NOT NULL,
  `continent` varchar(2) NOT NULL,
  `admin_area` varchar(2) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `iso2_code` (`iso2_code`),
  UNIQUE KEY `iso3_code` (`iso3_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `livesettings_longsetting`
--

DROP TABLE IF EXISTS `livesettings_longsetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `livesettings_longsetting` (
  `id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `group` varchar(100) NOT NULL,
  `key` varchar(100) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `site_id` (`site_id`,`group`,`key`),
  KEY `livesettings_longsetting_6223029` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `livesettings_setting`
--

DROP TABLE IF EXISTS `livesettings_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `livesettings_setting` (
  `id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `group` varchar(100) NOT NULL,
  `key` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `site_id` (`site_id`,`group`,`key`),
  KEY `livesettings_setting_6223029` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_creditcarddetail`
--

DROP TABLE IF EXISTS `payment_creditcarddetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_creditcarddetail` (
  `id` int(11) NOT NULL,
  `orderpayment_id` int(11) NOT NULL,
  `credit_type` varchar(16) NOT NULL,
  `display_cc` varchar(4) NOT NULL,
  `encrypted_cc` varchar(40) default NULL,
  `expire_month` int(11) NOT NULL,
  `expire_year` int(11) NOT NULL,
  `card_holder` varchar(60) NOT NULL,
  `start_month` int(11) default NULL,
  `start_year` int(11) default NULL,
  `issue_num` varchar(2) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `orderpayment_id` (`orderpayment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_paymentoption`
--

DROP TABLE IF EXISTS `payment_paymentoption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_paymentoption` (
  `id` int(11) NOT NULL,
  `description` varchar(20) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `optionName` varchar(20) NOT NULL,
  `sortOrder` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `optionName` (`optionName`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_attributeoption`
--

DROP TABLE IF EXISTS `product_attributeoption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_attributeoption` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `validation` varchar(100) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `error_message` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `product_attributeoption_52094d6e` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_category` (
  `id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `parent_id` int(11) default NULL,
  `meta` text,
  `description` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `site_id` (`site_id`,`slug`),
  KEY `product_category_6223029` (`site_id`),
  KEY `product_category_56ae2a2a` (`slug`),
  KEY `product_category_63f17a16` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_category_related_categories`
--

DROP TABLE IF EXISTS `product_category_related_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_category_related_categories` (
  `id` int(11) NOT NULL,
  `from_category_id` int(11) NOT NULL,
  `to_category_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `from_category_id` (`from_category_id`,`to_category_id`),
  KEY `product_category_related_categories_5809f13e` (`from_category_id`),
  KEY `product_category_related_categories_79bdfe15` (`to_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_categoryattribute`
--

DROP TABLE IF EXISTS `product_categoryattribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_categoryattribute` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `languagecode` varchar(10) default NULL,
  `option_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `product_categoryattribute_42dc49bc` (`category_id`),
  KEY `product_categoryattribute_2f3b0dc9` (`option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_categoryimage`
--

DROP TABLE IF EXISTS `product_categoryimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_categoryimage` (
  `id` int(11) NOT NULL,
  `category_id` int(11) default NULL,
  `picture` varchar(200) NOT NULL,
  `caption` varchar(100) default NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `category_id` (`category_id`,`sort`),
  KEY `product_categoryimage_42dc49bc` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_categoryimagetranslation`
--

DROP TABLE IF EXISTS `product_categoryimagetranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_categoryimagetranslation` (
  `id` int(11) NOT NULL,
  `categoryimage_id` int(11) NOT NULL,
  `languagecode` varchar(10) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `version` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `categoryimage_id` (`categoryimage_id`,`languagecode`,`version`),
  KEY `product_categoryimagetranslation_2ac39f6a` (`categoryimage_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_categorytranslation`
--

DROP TABLE IF EXISTS `product_categorytranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_categorytranslation` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `languagecode` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `version` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `category_id` (`category_id`,`languagecode`,`version`),
  KEY `product_categorytranslation_42dc49bc` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_discount`
--

DROP TABLE IF EXISTS `product_discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_discount` (
  `id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `code` varchar(20) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `amount` decimal(10,0) default NULL,
  `percentage` decimal(10,0) default NULL,
  `automatic` tinyint(1) default NULL,
  `allowedUses` int(11) default NULL,
  `numUses` int(11) default NULL,
  `minOrder` decimal(10,0) default NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `shipping` varchar(10) default NULL,
  `allValid` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `product_discount_6223029` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_discount_valid_categories`
--

DROP TABLE IF EXISTS `product_discount_valid_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_discount_valid_categories` (
  `id` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `discount_id` (`discount_id`,`category_id`),
  KEY `product_discount_valid_categories_5c536bf3` (`discount_id`),
  KEY `product_discount_valid_categories_42dc49bc` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_discount_valid_products`
--

DROP TABLE IF EXISTS `product_discount_valid_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_discount_valid_products` (
  `id` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `discount_id` (`discount_id`,`product_id`),
  KEY `product_discount_valid_products_5c536bf3` (`discount_id`),
  KEY `product_discount_valid_products_44bdf3ee` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_option`
--

DROP TABLE IF EXISTS `product_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_option` (
  `id` int(11) NOT NULL,
  `option_group_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL,
  `price_change` decimal(10,0) default NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `option_group_id` (`option_group_id`,`value`),
  KEY `product_option_2619a0bd` (`option_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_optiongroup`
--

DROP TABLE IF EXISTS `product_optiongroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_optiongroup` (
  `id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `product_optiongroup_6223029` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_optiongrouptranslation`
--

DROP TABLE IF EXISTS `product_optiongrouptranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_optiongrouptranslation` (
  `id` int(11) NOT NULL,
  `optiongroup_id` int(11) NOT NULL,
  `languagecode` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `version` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `optiongroup_id` (`optiongroup_id`,`languagecode`,`version`),
  KEY `product_optiongrouptranslation_2d4ce28b` (`optiongroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_optiontranslation`
--

DROP TABLE IF EXISTS `product_optiontranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_optiontranslation` (
  `id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `languagecode` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `version` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `option_id` (`option_id`,`languagecode`,`version`),
  KEY `product_optiontranslation_2f3b0dc9` (`option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_price`
--

DROP TABLE IF EXISTS `product_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_price` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `quantity` decimal(10,0) NOT NULL,
  `expires` date default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `product_id` (`product_id`,`quantity`,`expires`),
  KEY `product_price_44bdf3ee` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_product`
--

DROP TABLE IF EXISTS `product_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_product` (
  `id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `sku` varchar(255) default NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `items_in_stock` decimal(10,0) NOT NULL,
  `meta` text,
  `date_added` date default NULL,
  `active` tinyint(1) NOT NULL,
  `featured` tinyint(1) NOT NULL,
  `ordering` int(11) NOT NULL,
  `weight` decimal(10,0) default NULL,
  `weight_units` varchar(3) default NULL,
  `length` decimal(10,0) default NULL,
  `length_units` varchar(3) default NULL,
  `width` decimal(10,0) default NULL,
  `width_units` varchar(3) default NULL,
  `height` decimal(10,0) default NULL,
  `height_units` varchar(3) default NULL,
  `total_sold` decimal(10,0) NOT NULL,
  `taxable` tinyint(1) NOT NULL,
  `taxClass_id` int(11) default NULL,
  `shipclass` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `site_id_2` (`site_id`,`slug`),
  UNIQUE KEY `site_id` (`site_id`,`sku`),
  KEY `product_product_6223029` (`site_id`),
  KEY `product_product_56ae2a2a` (`slug`),
  KEY `product_product_21ec4f69` (`taxClass_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_product_also_purchased`
--

DROP TABLE IF EXISTS `product_product_also_purchased`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_product_also_purchased` (
  `id` int(11) NOT NULL,
  `from_product_id` int(11) NOT NULL,
  `to_product_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `from_product_id` (`from_product_id`,`to_product_id`),
  KEY `product_product_also_purchased_377b069c` (`from_product_id`),
  KEY `product_product_also_purchased_5bd7a0dd` (`to_product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_product_category`
--

DROP TABLE IF EXISTS `product_product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_product_category` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `product_id` (`product_id`,`category_id`),
  KEY `product_product_category_44bdf3ee` (`product_id`),
  KEY `product_product_category_42dc49bc` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_product_related_items`
--

DROP TABLE IF EXISTS `product_product_related_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_product_related_items` (
  `id` int(11) NOT NULL,
  `from_product_id` int(11) NOT NULL,
  `to_product_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `from_product_id` (`from_product_id`,`to_product_id`),
  KEY `product_product_related_items_377b069c` (`from_product_id`),
  KEY `product_product_related_items_5bd7a0dd` (`to_product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_productattribute`
--

DROP TABLE IF EXISTS `product_productattribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_productattribute` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `languagecode` varchar(10) default NULL,
  `option_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `product_productattribute_44bdf3ee` (`product_id`),
  KEY `product_productattribute_2f3b0dc9` (`option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_productimage`
--

DROP TABLE IF EXISTS `product_productimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_productimage` (
  `id` int(11) NOT NULL,
  `product_id` int(11) default NULL,
  `picture` varchar(200) NOT NULL,
  `caption` varchar(100) default NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `product_productimage_44bdf3ee` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_productimagetranslation`
--

DROP TABLE IF EXISTS `product_productimagetranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_productimagetranslation` (
  `id` int(11) NOT NULL,
  `productimage_id` int(11) NOT NULL,
  `languagecode` varchar(10) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `version` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `productimage_id` (`productimage_id`,`languagecode`,`version`),
  KEY `product_productimagetranslation_2f19b5e4` (`productimage_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_productpricelookup`
--

DROP TABLE IF EXISTS `product_productpricelookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_productpricelookup` (
  `id` int(11) NOT NULL,
  `siteid` int(11) NOT NULL,
  `key` varchar(60) default NULL,
  `parentid` int(11) default NULL,
  `productslug` varchar(255) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `quantity` decimal(10,0) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `discountable` tinyint(1) NOT NULL,
  `items_in_stock` decimal(10,0) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `product_productpricelookup_693949e4` (`productslug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_producttranslation`
--

DROP TABLE IF EXISTS `product_producttranslation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_producttranslation` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `languagecode` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `short_description` text NOT NULL,
  `version` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `product_id` (`product_id`,`languagecode`,`version`),
  KEY `product_producttranslation_44bdf3ee` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_taxclass`
--

DROP TABLE IF EXISTS `product_taxclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_taxclass` (
  `id` int(11) NOT NULL,
  `title` varchar(20) NOT NULL,
  `description` varchar(30) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `registration_registrationprofile`
--

DROP TABLE IF EXISTS `registration_registrationprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration_registrationprofile` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `activation_key` varchar(40) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_cart`
--

DROP TABLE IF EXISTS `shop_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_cart` (
  `id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `desc` varchar(10) default NULL,
  `date_time_created` datetime NOT NULL,
  `customer_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `shop_cart_6223029` (`site_id`),
  KEY `shop_cart_12366e04` (`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_cartitem`
--

DROP TABLE IF EXISTS `shop_cartitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_cartitem` (
  `id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` decimal(10,0) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `shop_cartitem_129fc6a` (`cart_id`),
  KEY `shop_cartitem_44bdf3ee` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_cartitemdetails`
--

DROP TABLE IF EXISTS `shop_cartitemdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_cartitemdetails` (
  `id` int(11) NOT NULL,
  `cartitem_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `name` varchar(100) NOT NULL,
  `price_change` decimal(10,0) default NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `shop_cartitemdetails_28dee0a7` (`cartitem_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_config`
--

DROP TABLE IF EXISTS `shop_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_config` (
  `site_id` int(11) NOT NULL,
  `store_name` varchar(100) NOT NULL,
  `store_description` text,
  `store_email` varchar(75) default NULL,
  `street1` varchar(50) default NULL,
  `street2` varchar(50) default NULL,
  `city` varchar(50) default NULL,
  `state` varchar(30) default NULL,
  `postal_code` varchar(9) default NULL,
  `country_id` int(11) NOT NULL,
  `phone` varchar(30) default NULL,
  `in_country_only` tinyint(1) NOT NULL,
  `sales_country_id` int(11) default NULL,
  PRIMARY KEY  (`site_id`),
  UNIQUE KEY `store_name` (`store_name`),
  KEY `shop_config_534dd89` (`country_id`),
  KEY `shop_config_7724117a` (`sales_country_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_config_shipping_countries`
--

DROP TABLE IF EXISTS `shop_config_shipping_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_config_shipping_countries` (
  `id` int(11) NOT NULL,
  `config_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `config_id` (`config_id`,`country_id`),
  KEY `shop_config_shipping_countries_c41bdac` (`config_id`),
  KEY `shop_config_shipping_countries_534dd89` (`country_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_order`
--

DROP TABLE IF EXISTS `shop_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_order` (
  `id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `ship_addressee` varchar(61) NOT NULL,
  `ship_street1` varchar(80) NOT NULL,
  `ship_street2` varchar(80) NOT NULL,
  `ship_city` varchar(50) NOT NULL,
  `ship_state` varchar(50) NOT NULL,
  `ship_postal_code` varchar(30) NOT NULL,
  `ship_country` varchar(2) NOT NULL,
  `bill_addressee` varchar(61) NOT NULL,
  `bill_street1` varchar(80) NOT NULL,
  `bill_street2` varchar(80) NOT NULL,
  `bill_city` varchar(50) NOT NULL,
  `bill_state` varchar(50) NOT NULL,
  `bill_postal_code` varchar(30) NOT NULL,
  `bill_country` varchar(2) NOT NULL,
  `notes` text,
  `sub_total` decimal(10,0) default NULL,
  `total` decimal(10,0) default NULL,
  `discount_code` varchar(20) default NULL,
  `discount` decimal(10,0) default NULL,
  `method` varchar(50) NOT NULL,
  `shipping_description` varchar(50) default NULL,
  `shipping_method` varchar(50) default NULL,
  `shipping_model` varchar(30) default NULL,
  `shipping_cost` decimal(10,0) default NULL,
  `shipping_discount` decimal(10,0) default NULL,
  `tax` decimal(10,0) default NULL,
  `time_stamp` datetime default NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `shop_order_6223029` (`site_id`),
  KEY `shop_order_170b8823` (`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_orderauthorization`
--

DROP TABLE IF EXISTS `shop_orderauthorization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_orderauthorization` (
  `id` int(11) NOT NULL,
  `payment` varchar(25) NOT NULL,
  `amount` decimal(10,0) default NULL,
  `time_stamp` datetime default NULL,
  `transaction_id` varchar(45) default NULL,
  `details` varchar(255) default NULL,
  `reason_code` varchar(255) default NULL,
  `order_id` int(11) NOT NULL,
  `capture_id` int(11) NOT NULL,
  `complete` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `shop_orderauthorization_7cc8fcf5` (`order_id`),
  KEY `shop_orderauthorization_13f21dcf` (`capture_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_orderitem`
--

DROP TABLE IF EXISTS `shop_orderitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_orderitem` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` decimal(10,0) NOT NULL,
  `unit_price` decimal(10,0) NOT NULL,
  `unit_tax` decimal(10,0) NOT NULL,
  `line_item_price` decimal(10,0) NOT NULL,
  `tax` decimal(10,0) NOT NULL,
  `expire_date` date default NULL,
  `completed` tinyint(1) NOT NULL,
  `discount` decimal(10,0) default NULL,
  PRIMARY KEY  (`id`),
  KEY `shop_orderitem_7cc8fcf5` (`order_id`),
  KEY `shop_orderitem_44bdf3ee` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_orderitemdetail`
--

DROP TABLE IF EXISTS `shop_orderitemdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_orderitemdetail` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL,
  `price_change` decimal(10,0) default NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `shop_orderitemdetail_67b70d25` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_orderpayment`
--

DROP TABLE IF EXISTS `shop_orderpayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_orderpayment` (
  `id` int(11) NOT NULL,
  `payment` varchar(25) NOT NULL,
  `amount` decimal(10,0) default NULL,
  `time_stamp` datetime default NULL,
  `transaction_id` varchar(45) default NULL,
  `details` varchar(255) default NULL,
  `reason_code` varchar(255) default NULL,
  `order_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `shop_orderpayment_7cc8fcf5` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_orderpaymentfailure`
--

DROP TABLE IF EXISTS `shop_orderpaymentfailure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_orderpaymentfailure` (
  `id` int(11) NOT NULL,
  `payment` varchar(25) NOT NULL,
  `amount` decimal(10,0) default NULL,
  `time_stamp` datetime default NULL,
  `transaction_id` varchar(45) default NULL,
  `details` varchar(255) default NULL,
  `reason_code` varchar(255) default NULL,
  `order_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `shop_orderpaymentfailure_7cc8fcf5` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_orderpendingpayment`
--

DROP TABLE IF EXISTS `shop_orderpendingpayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_orderpendingpayment` (
  `id` int(11) NOT NULL,
  `payment` varchar(25) NOT NULL,
  `amount` decimal(10,0) default NULL,
  `time_stamp` datetime default NULL,
  `transaction_id` varchar(45) default NULL,
  `details` varchar(255) default NULL,
  `reason_code` varchar(255) default NULL,
  `order_id` int(11) NOT NULL,
  `capture_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `shop_orderpendingpayment_7cc8fcf5` (`order_id`),
  KEY `shop_orderpendingpayment_13f21dcf` (`capture_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_orderstatus`
--

DROP TABLE IF EXISTS `shop_orderstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_orderstatus` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `notes` varchar(100) NOT NULL,
  `time_stamp` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `shop_orderstatus_7cc8fcf5` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_ordertaxdetail`
--

DROP TABLE IF EXISTS `shop_ordertaxdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_ordertaxdetail` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `method` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `tax` decimal(10,0) default NULL,
  PRIMARY KEY  (`id`),
  KEY `shop_ordertaxdetail_7cc8fcf5` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_ordervariable`
--

DROP TABLE IF EXISTS `shop_ordervariable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_ordervariable` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `key` varchar(50) NOT NULL,
  `value` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `shop_ordervariable_7cc8fcf5` (`order_id`),
  KEY `shop_ordervariable_45544485` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-11-14 16:31:51
