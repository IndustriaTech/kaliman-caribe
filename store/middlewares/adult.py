from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.conf import settings

def allow_bots(func):
    def wrapper(self, request, *args, **kwargs):
        user_agent = request.META['HTTP_USER_AGENT']
        if "bot" not in user_agent.lower():
            return func(self, request, *args, **kwargs)
    return wrapper

class AdultsOnlyMiddleware(object):
    """
        Each user has to confirm that he is at least 18 years old

        Note: No, I did not make this for some porn site.
    """
    url = reverse('confirm_your_age')
    adult_key = 'has_18'
    safe_urls = [url,
    		 "/confirm_your_age",
    		 "/bg/confirm_your_age",
    		 "/en/confirm_your_age",
                 "/sorry/",
                 "/bg/sorry/",
                 "/en/sorry/",
                 "/sitemap.xml",
                 "/en/sitemap.xml",
                 "/bg/sitemap.xml",
                 "/robots.txt",
                ]

    @allow_bots
    def process_request(self, request):
        if not request.session.get(self.adult_key, False):
            if request.GET.get(self.adult_key, False):
                request.session[self.adult_key] = True
                return redirect(request.GET.get('next', '/') or '/')
            if request.path_info not in self.safe_urls and settings.MEDIA_URL not in request.path_info:
                return redirect( "%s?next=%s" % (self.url, request.path_info))
        elif request.path_info in self.safe_urls:
            return redirect('/')

