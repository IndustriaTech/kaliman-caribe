"""
This is a stub and used as the default processor.
It doesn't do anything but it can be used to build out another
interface.

See the authorizenet module for the reference implementation
"""
from django.utils.translation import ugettext_lazy as _
from payment.modules.base import BasePaymentProcessor, ProcessorResult

class PaymentProcessor(BasePaymentProcessor):

    def __init__(self, settings):
        super(PaymentProcessor, self).__init__('email', settings)

    def authorize_payment(self, order=None, testing=False, amount=None):
        """
        Make an authorization for an order.  This payment will then be captured when the order
        is set marked 'shipped'.
        """
        if order == None:
            order = self.order

        if amount is None:
            amount = order.balance

        orderauth = self.record_authorization(amount=amount, reason_code="0")
        return ProcessorResult(self.key, True, _('Success'), orderauth)

    def can_authorize(self):
        return True

    def capture_payment(self, testing=False, amount=None):
        """
        Process the transaction and return a ProcessorResult:

        Example:
        >>> from livesettings import config_get_group
        >>> settings = config_get_group('PAYMENT_EMAIL')
        >>> from payment.modules.email.processor import PaymentProcessor
        >>> processor = PaymentProcessor(settings)
        # If using a normal payment module, data should be an Order object.
        >>> data = {}
        >>> processor.prepare_data(data)
        >>> processor.process()
        ProcessorResult: EMAIL [Success] Success
        """

        orderpayment = self.record_payment(amount=amount, reason_code="0")
        return ProcessorResult(self.key, True, _('Success'), orderpayment)


    def capture_authorized_payment(self, authorization, amount=None):
        """
        Given a prior authorization, capture remaining amount not yet captured.
        """
        if amount is None:
            amount = authorization.remaining()

        orderpayment = self.record_payment(amount=amount, reason_code="0",
            transaction_id="email", authorization=authorization)

        return ProcessorResult(self.key, True, _('Success'), orderpayment)

    def release_authorized_payment(self, order=None, auth=None, testing=False):
        """Release a previously authorized payment."""
        auth.complete = True
        auth.save()
        return ProcessorResult(self.key, True, _('Success'))
