# -*- coding: utf-8 -*-
from django.contrib import admin
from models import Country, City, Store

from hvad.admin import TranslatableAdmin

class StoreAdmin(TranslatableAdmin):
    list_display = ('name', 'country', 'city')

admin.site.register(Country, TranslatableAdmin)
admin.site.register(City, TranslatableAdmin)
admin.site.register(Store, StoreAdmin)

