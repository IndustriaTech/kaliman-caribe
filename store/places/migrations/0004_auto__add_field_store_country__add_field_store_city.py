# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'Store.country'
        db.add_column('places_store', 'country', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['places.Country']), keep_default=False)

        # Adding field 'Store.city'
        db.add_column('places_store', 'city', self.gf('django.db.models.fields.related.ForeignKey')(default=0, related_name='city', to=orm['places.City']), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'Store.country'
        db.delete_column('places_store', 'country_id')

        # Deleting field 'Store.city'
        db.delete_column('places_store', 'city_id')


    models = {
        'places.city': {
            'Meta': {'object_name': 'City'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'country'", 'to': "orm['places.Country']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'})
        },
        'places.country': {
            'Meta': {'object_name': 'Country'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'})
        },
        'places.store': {
            'Meta': {'object_name': 'Store'},
            'can_buy': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'can_smoke': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'city'", 'to': "orm['places.City']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['places.Country']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['places']
