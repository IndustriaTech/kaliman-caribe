# encoding: utf-8
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        for country in orm.Country.objects.all():
            translated_country = orm.CountryTranslation(
                master = country,
                language_code = 'bg',
                name = country.name_bg
            )
            translated_country.save()


    def backwards(self, orm):
        for translated_country in orm.CountryTranslation.objects.filter(language_code='bg'):
            translated_country.master.title_bg = translated_country.title
            translated_country.master.save()


    models = {
        'places.city': {
            'Meta': {'unique_together': "(('country', 'name'),)", 'object_name': 'City'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['places.Country']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32', 'blank': 'True'})
        },
        'places.country': {
            'Meta': {'object_name': 'Country'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_bg': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'})
        },
        'places.countrytranslation': {
            'Meta': {'unique_together': "[('language_code', 'master')]", 'object_name': 'CountryTranslation', 'db_table': "'places_country_translation'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'master': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': "orm['places.Country']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'})
        },
        'places.store': {
            'Meta': {'object_name': 'Store'},
            'address_link': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'can_buy': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'can_smoke': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['places.City']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['places.Country']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['places']
