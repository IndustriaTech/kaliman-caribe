# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Removing unique constraint on 'City', fields ['country', 'name']
        db.delete_unique('places_city', ['country_id', 'name'])

        # Adding model 'CityTranslation'
        db.create_table('places_city_translation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32, blank=True)),
            ('language_code', self.gf('django.db.models.fields.CharField')(max_length=15, db_index=True)),
            ('master', self.gf('django.db.models.fields.related.ForeignKey')(related_name='translations', null=True, to=orm['places.City'])),
        ))
        db.send_create_signal('places', ['CityTranslation'])

        # Adding unique constraint on 'CityTranslation', fields ['language_code', 'master']
        db.create_unique('places_city_translation', ['language_code', 'master_id'])

        # Deleting field 'City.name'
        db.delete_column('places_city', 'name')


    def backwards(self, orm):
        
        # Removing unique constraint on 'CityTranslation', fields ['language_code', 'master']
        db.delete_unique('places_city_translation', ['language_code', 'master_id'])

        # Deleting model 'CityTranslation'
        db.delete_table('places_city_translation')

        # Adding field 'City.name'
        db.add_column('places_city', 'name', self.gf('django.db.models.fields.CharField')(default='', max_length=32, blank=True), keep_default=False)

        # Adding unique constraint on 'City', fields ['country', 'name']
        db.create_unique('places_city', ['country_id', 'name'])


    models = {
        'places.city': {
            'Meta': {'object_name': 'City'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['places.Country']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_bg': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'})
        },
        'places.citytranslation': {
            'Meta': {'unique_together': "[('language_code', 'master')]", 'object_name': 'CityTranslation', 'db_table': "'places_city_translation'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'master': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': "orm['places.City']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'})
        },
        'places.country': {
            'Meta': {'object_name': 'Country'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'places.countrytranslation': {
            'Meta': {'unique_together': "[('language_code', 'master')]", 'object_name': 'CountryTranslation', 'db_table': "'places_country_translation'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'master': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': "orm['places.Country']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'})
        },
        'places.store': {
            'Meta': {'object_name': 'Store'},
            'address_link': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'can_buy': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'can_smoke': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['places.City']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['places.Country']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['places']
