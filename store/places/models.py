# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _

from hvad.models import TranslatableModel, TranslatedFields
from hvad.manager import TranslationManager

class Country(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(max_length=32, blank=True),
    )

    class Meta:
        verbose_name = _("country")
        verbose_name_plural = _("countries")

    def __unicode__(self):
        return self.name


class City(TranslatableModel):
    country = models.ForeignKey(Country, blank=False, null=False)
    ordering = models.PositiveIntegerField(default=0)

    translations = TranslatedFields(
        name = models.CharField(max_length=32, blank=True),
    )


    class Meta:
        verbose_name = _("city")
        verbose_name_plural = _("cities")
        ordering = ('ordering', )

    def __unicode__(self):
        return self.name

    # @models.permalink
    # def get_absolute_url(self):
    #     return ('view_or_url_name' )

class FeaturedStores(TranslationManager):
    def get_query_set(self):
        return super(FeaturedStores, self).get_query_set().filter(is_featured=True)

class ActiveStores(TranslationManager):
    def get_query_set(self):
        return super(ActiveStores, self).get_query_set().filter(is_active=True)

class Store(TranslatableModel):
    """List with all the stores"""

    name = models.CharField(max_length=64, verbose_name=_("Name"))
    country = models.ForeignKey(Country, blank=False, null=False, verbose_name=_('Country'))
    city = models.ForeignKey(City, blank=False, null=False, verbose_name=_('City'))
    address_link = models.CharField(_("Google Maps Location"), max_length=256, blank=True)
    photo = models.ImageField(upload_to="stores", verbose_name=_("Photo"), blank=True)
    can_buy = models.BooleanField(_("Can buy"), default=True)
    can_smoke = models.BooleanField(_("Can smoke"), default=False)
    is_featured = models.BooleanField(_("Featured"), default=False)
    is_active = models.BooleanField(_("Active"), default=True)
    ordering = models.PositiveIntegerField(default=0)

    translations = TranslatedFields(
        description = models.TextField(_("Description"))
    )

    objects = TranslationManager()
    featured = FeaturedStores()
    active = ActiveStores()

    class Meta:
        verbose_name = _("store")
        verbose_name_plural = _("stores")
        ordering = ('ordering', )

    def __unicode__(self):
        return self.name
