# -*- encoding: utf-8 -*-
from django.template.context import RequestContext
from django.shortcuts import render_to_response

from models import Store
from utils import OrderedSet

def all_stores():
    """
    TODO: This should be a template tag.
    """
    return Store.active.all()

def left_menu():
    """
    TODO: This should be a template tag.
    TODO: Think about cache.
    """
    sections = {'can_buy': {}, 'can_smoke': {}}
    all_stores = Store.active.select_related('country', 'city').all().order_by('city__ordering')

    for store in all_stores:
        for section in sections:
            if store.__dict__[section]:
                sections[section].setdefault(store.country, OrderedSet()).add(store.city)
    return sections

def index(request, activity="can_buy", city_id=0):
    sections = left_menu()
    if not city_id:
        stores = all_stores()
        return render_to_response('places/map.html', locals(),
                                  context_instance=RequestContext(request))
    else:
        kw = {'city__id': city_id, activity: True}
        stores = Store.active.select_related('city').filter(**kw)
        city_name = stores[0].city.name
        return render_to_response('places/index.html', locals(),
                                  context_instance=RequestContext(request))
