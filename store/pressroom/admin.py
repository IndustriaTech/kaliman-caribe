from django.contrib import admin
from django.db.models import TextField

from tinymce.widgets import TinyMCE
from hvad.admin import TranslatableAdmin

from models import Article, Document, Section

class ArticleAdmin(TranslatableAdmin):
    list_display = ('__unicode__', 'author', 'pub_date', 'publish')
    formfield_overrides = {TextField: {'widget': TinyMCE(attrs={'cols': 60, 'rows': 20}, )}}
    list_filter = ['pub_date', 'sections']
    date_hierarchy = 'pub_date'
    save_as = True

    def __init__(self, *args, **kwargs):
        super(ArticleAdmin, self).__init__(*args, **kwargs)
        self.prepopulated_fields = {'slug': ('headline',)}

class DocumentAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title', 'pub_date')
    list_filter = ['pub_date']

class SectionAdmin(TranslatableAdmin):
    list_display = ('__unicode__',)

    def __init__(self, *args, **kwargs):
        super(SectionAdmin, self).__init__(*args, **kwargs)
        self.prepopulated_fields = {'slug': ('title',)}

admin.site.register(Article, ArticleAdmin)
admin.site.register(Document, DocumentAdmin)
admin.site.register(Section, SectionAdmin)
