from django.core.cache import cache
from django.contrib.sites.models import Site
from django.template import Library, Node, Variable
from django.template import TemplateSyntaxError, VariableDoesNotExist
from pressroom.models import Section
from satchmo_utils.templatetags import get_filter_args
from django.utils.translation import get_language

register = Library()

@register.assignment_tag
def list_sections():
    return Section.objects.all().order_by('id')
