import re

from django import template
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe

register = template.Library()

def get_youtube_id(url):
    """ Fetches the video_id from it's URL """
    pattern = re.compile(r"^(http://)?(www\.)?(youtube\.[a-z]{2,4}/watch\?v=)?(?P<id>[A-Za-z0-9\-=_]{11})(.*)")
    match = pattern.match(url)
    if match:
        return match.group('id')

@register.filter
@stringfilter
def youtube_player(url):
    """ YouTube embed code """
    video_id = get_youtube_id(url)
    if not video_id:
        return ""
    return mark_safe('<iframe width="560" height="315" src="http://www.youtube.com/embed/%s" frameborder="0" allowfullscreen></iframe>' % video_id)

@register.filter
@stringfilter
def youtube_thumbnail(url, size='small'):
    """ Returns YouTube thumb url """
    video_id = get_youtube_id(url)
    if size == 'small':
        return "http://img.youtube.com/vi/%s/1.jpg" % video_id
    elif size == 'large':
        return "http://img.youtube.com/vi/%s/0.jpg" % video_id
