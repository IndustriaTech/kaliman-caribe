# -*- encoding: utf-8 -*-
from datetime import datetime

from django.template.context import RequestContext
from django.views.generic import list_detail
from django.shortcuts import render_to_response, get_object_or_404

from pressroom.models import Article, Section, Document
from satchmo_ext.newsletter.models import HabanosNewsletter


def index(request):
    sections = Section.objects.all().order_by('id')
    news_section = Section.objects.language().filter(pk=1)
    articles = Article.objects.get_published().filter(sections=news_section)
    featured  = Article.objects.get_published().filter(featured=True)
    try:
        from photologue.models import Gallery
        galleries = Gallery.objects.all()[:3]
    except:
        pass
    return render_to_response('pressroom/index.html', locals(),
                              context_instance=RequestContext(request))


def view_section(request, slug, page=1):
    featured  = Article.objects.get_published().filter(featured=True)
    sections = Section.objects.all().order_by('id')
    section = get_object_or_404(Section, slug=slug)
    if section.slug == "newsletter":
        articles = HabanosNewsletter.published.all()
    else:
        articles = section.articles.filter(publish=True, pub_date__lte=datetime.now())
    try:
        from photologue.models import Gallery
        galleries = Gallery.objects.all()[:3]
    except:
        galleries = None
    return list_detail.object_list(request,
                                   queryset=articles,
                                   paginate_by=5,
                                   page=page,
                                   allow_empty=True,
                                   template_name='pressroom/index.html',
                                   extra_context={'sections': sections,
                                                  'section': section,
                                                  'articles': articles,
                                                  'featured': featured,
                                                  'galleries': Gallery.objects.all()[:3]})

def article_list(request, page=0):
    return list_detail.object_list(request=request,
                                   queryset=Article.objects.get_published(),
                                   allow_empty=True,
                                   paginate_by=5,
                                   page=page)
