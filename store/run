#!/bin/bash
#################################################################
# Startup script for Django server, that will be placed
# in /usr/local/bin for example. It may be symlinked
# in project dir with name "run" like:
#    ln -s /usr/local/bin/django-site run.
#
# Usage as global command:
#   django-site [path/to/project] <start|stop|restart>
#
# Usage as symlinked script placed in project root:
#   ./run <start|stop|restart>
#
# Supports runfcgi and run_gunicorn django servers
# Can load configuration file if exists named django-server.conf
#
# created by Venelin Stoykov vkstoykov@gmail.com
#################################################################

# Default parameters that may be configured
# in project direcotry with file django-site.conf

MIN_SPARE=1             # Minimum spares for the project
MAX_SPARE=1             # Maximum spares for the project
MAX_CHILDREN=2

MANAGE="manage.py"      # Script that manage project
INTERPRETER="/usr/bin/python2.7"    # MANAGE interpreter

HOST="127.0.0.1"
SERVER="runfcgi"         # Can be "runfcgi" or "run_gunicorn"


# ============================================================
# Setting Project dir and Project name from passed argument
# or location of script or the current directory
# ============================================================
if [ -d "$1" ]; then
    PROJDIR=`cd $1; pwd`
    shift
elif [ "`basename $0`" == "run" ]; then
    reldir=`dirname $0`
    PROJDIR=`cd $reldir; pwd`
else
    PROJDIR=$PWD
fi

OWNER=`ls -ld $PROJDIR | awk '{print $3}'`

PROJNAME=`basename $PROJDIR`
PIDFILE="$PROJDIR/$PROJNAME.pid"
SOCKET="$PROJDIR/fcgi.sock"
LOGFILE="$PROJDIR/$PROJNAME.log"

[ -f "$PROJDIR/django-site.conf" ] && source "$PROJDIR/django-site.conf"

# =============================================
#  End of configuration. Begin the real script
# =============================================

function get_PID {
    if [ -f $PIDFILE ]; then
        local pid=$(ps ax | grep `cat -- $PIDFILE` | grep $PROJNAME | grep -v "grep" | awk '{print $1}')
        [ ! -z "$pid" ] && echo $pid && return
    fi
    echo 0
}

function remove_files {
    [ -f $PIDFILE ] &&  rm -f -- $PIDFILE
    [ -S $SOCKET ] && rm -f -- $SOCKET
}

function stop_project {
    echo "Stopping ${PROJNAME}..."

    local pid=$(get_PID)
    [ $pid -gt 0 ] && kill $pid

    sleep 1

    remove_files
}

function start_project {
    if [ ! -f $PROJDIR/$MANAGE ]; then
        echo "Selected path is not a django project"
        exit 1
    fi

    if [ $(get_PID) -gt 0  ]; then
        echo "The project ${PROJNAME} is started. Try restart instead."
        exit 2
    fi
    echo "Starting ${PROJNAME}..."

    case $SERVER in
        runfcgi)
            PARAMETERS="pidfile='$PIDFILE' minspare=$MIN_SPARE maxspare=$MAX_SPARE maxchildren=$MAX_CHILDREN"
            if [ -z "$PORT" ]; then
                SERVER_COMMAND="runfcgi socket='$SOCKET' $PARAMETERS"
            else
                SERVER_COMMAND="runfcgi host=$HOST port=$PORT $PARAMETERS"
            fi
        ;;
        run_gunicorn)
            PARAMETERS="--pid='$PIDFILE' -D --log-file='$LOGFILE'"
            if [ -z "$PORT" ]; then
                SERVER_COMMAND="run_gunicorn unix:$SOCKET $PARAMETERS"
            else
                SERVER_COMMAND="run_gunicorn $HOST:$PORT $PARAMETERS"
            fi
        ;;
        *)
            if [ -z "$SERVER_COMMAND" ]; then
                echo "Unknown server!!! Pleace set SERVER_COMMAND in django-site.conf to run it!"
                exit 3
            fi

        ;;
    esac
    COMMAND="$INTERPRETER '$PROJDIR/$MANAGE' $SERVER_COMMAND"
    #echo $COMMAND

    if [ $OWNER = $(whoami) ]; then
      sh -c "$COMMAND"
    else
      su $OWNER -c "$COMMAND"
    fi

    # If not using socket try several times to change socket permisions
    # to grant access for web server (nginx, apache)
    if [ -z "$PORT" ]; then
        for i in 1 2 3 4 5; do
            if [ -S $SOCKET ]; then
              chmod 777 $SOCKET
              break
            else
              echo "The socket was not created! Retrying after 1 second..."
              sleep 1
            fi
        done
    fi
}

case $1 in

    stop)
      stop_project
    ;;

    start)
      start_project
    ;;

    restart)
      stop_project
      sleep 1
      start_project
    ;;

    *)
      echo "Bad usage."
      echo "Use: `basename $0` [project_path] <start|stop|restart>"
    ;;

esac

exit 0
