from satchmo_ext.newsletter.models import Subscription, SubscriptionAttribute, HabanosNewsletter
from django.contrib import admin
from django.utils.translation import get_language, ugettext_lazy as _
    
class SubscriptionAttributeInline(admin.TabularInline):
    model = SubscriptionAttribute
    extra = 1

class SubscriptionOptions(admin.ModelAdmin):
    list_display = ['email', 'subscribed', 'create_date', 'update_date']
    list_filter = ['subscribed']
    inlines = [SubscriptionAttributeInline,]

class HabanosNewsletterAdmin(admin.ModelAdmin):
    list_display = ['name', 'has_bg_file', 'has_en_file', 'uploaded', 'is_published']

    def has_bg_file(self, obj):
        return obj.bg_file is not None
    has_bg_file.boolean = True

    def has_en_file(self, obj):
        return obj.en_file is not None
    has_en_file.boolean = True

admin.site.register(Subscription, SubscriptionOptions)
admin.site.register(HabanosNewsletter, HabanosNewsletterAdmin)

