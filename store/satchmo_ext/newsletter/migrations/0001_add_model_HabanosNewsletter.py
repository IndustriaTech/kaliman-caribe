# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'HabanosNewsletter'
        db.create_table('newsletter_habanosnewsletter', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('bg_file', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('en_file', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('uploaded', self.gf('django.db.models.fields.DateField')(default=datetime.date(2012, 2, 24), blank=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('newsletter', ['HabanosNewsletter'])


    def backwards(self, orm):
        
        # Deleting model 'HabanosNewsletter'
        db.delete_table('newsletter_habanosnewsletter')


    models = {
        'newsletter.habanosnewsletter': {
            'Meta': {'object_name': 'HabanosNewsletter'},
            'bg_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'en_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'uploaded': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2012, 2, 24)', 'blank': 'True'})
        },
        'newsletter.subscription': {
            'Meta': {'object_name': 'Subscription'},
            'create_date': ('django.db.models.fields.DateField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subscribed': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'update_date': ('django.db.models.fields.DateField', [], {})
        },
        'newsletter.subscriptionattribute': {
            'Meta': {'object_name': 'SubscriptionAttribute'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.SlugField', [], {'max_length': '100', 'db_index': 'True'}),
            'subscription': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'attributes'", 'to': "orm['newsletter.Subscription']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['newsletter']
