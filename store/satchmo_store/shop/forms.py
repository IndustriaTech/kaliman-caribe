# -*- coding: utf-8 -*-
from decimal import Decimal
from django import forms
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _
from livesettings import config_value
from product.models import Product
from satchmo_store.shop.signals import satchmo_cart_details_query, satchmo_cart_add_complete
from satchmo_utils.numbers import RoundedDecimalError, round_decimal, PositiveRoundedDecimalField
import logging

log = logging.getLogger('shop.forms')

class AdvancedSearchForm(forms.Form):
    """Too much choices for plain html"""
    PRICE_CHOICES = (
            ("0|12.01", _(u"До 12.00 лв")),
            ("12.01|20.01", _(u"12.00 - 20.00лв")),
            ("20.01|30.01", _(u"20.00 - 30.00лв")),
            ("30.01|infinity", _(u"Над 30.00лв")),
            ("", _(u"Всички")),
        )

    RING_CHOICES = (
            ("0|41", _(u"До 40")),
            ("42|49", _(u"42 - 48")),
            ("50|infinity", _(u"Над 50")),
            ("", _(u"Всички")),
        )

    LENGTH_CHOICES = (
            ("0|121", _(u"До 120мм")),
            ("124|141", _(u"124 - 140мм")),
            ("142|infinity", _(u"Над 142мм")),
            ("", _(u"Всички")),
        )

    STRENGTH_CHOICES = (
            (_(u"Лека"), _(u"Лека")),
            (_(u"Средна към лека"), _(u"Средна към лека")),
            (_(u"Средна"), _(u"Средна")),
            (_(u"Силна към средна"), _(u"Силна към средна")),
            (_(u"Силна"), _(u"Силна")),
            (u"", _(u"Всички")),
        )

    keywords = forms.CharField(max_length=255, required=False)
    price = forms.ChoiceField(widget=forms.RadioSelect, choices=PRICE_CHOICES, initial=PRICE_CHOICES[-1], required=False)
    ring = forms.ChoiceField(widget=forms.RadioSelect, choices=RING_CHOICES, initial=RING_CHOICES[-1], required=False)
    length = forms.ChoiceField(widget=forms.RadioSelect, choices=LENGTH_CHOICES, initial=LENGTH_CHOICES[-1], required=False)
    strength = forms.ChoiceField(widget=forms.RadioSelect, choices=STRENGTH_CHOICES, initial=LENGTH_CHOICES[-1], required=False)

    def __init__(self, *args, **kwargs):
        super(AdvancedSearchForm, self).__init__(*args, **kwargs)
        self.fields['keywords'].widget.attrs['placeholder'] = u"Търси в сайта"


class MultipleProductForm(forms.Form):
    """A form used to add multiple products to the cart."""

    def __init__(self, *args, **kwargs):
        products = kwargs.pop('products', None)

        super(MultipleProductForm, self).__init__(*args, **kwargs)

        if products:
            products = [p for p in products if p.active]
        else:
            products = Product.objects.active_by_site()

        self.slugs = [p.slug for p in products]

        for product in products:
            kw = {
                'label' : product.name,
                'help_text' : product.description,
                'initial' : 0,
                'widget' : forms.TextInput(attrs={'class': 'text'}),
                'required' : False
            }

            qty = PositiveRoundedDecimalField(**kw)
            qty.product = product
            self.fields['qty__%s' % product.slug] = qty

    def save(self, cart, request):
        log.debug('saving');
        self.full_clean()
        cartplaces = config_value('SHOP', 'CART_PRECISION')
        roundfactor = config_value('SHOP', 'CART_ROUNDING')
        site = Site.objects.get_current()

        for name, value in self.cleaned_data.items():
            opt, key = name.split('__')
            log.debug('%s=%s', opt, key)

            quantity = 0
            product = None

            if opt=='qty':
                try:
                    quantity = round_decimal(value, places=cartplaces, roundfactor=roundfactor)
                except RoundedDecimalError:
                    quantity = 0

            if not key in self.slugs:
                log.debug('caught attempt to add product not in the form: %s', key)
            else:
                try:
                    product = Product.objects.get(slug=key, site=site)
                except Product.DoesNotExist:
                    log.debug('caught attempt to add an non-existent product, ignoring: %s', key)

            if product and quantity > Decimal('0'):
                log.debug('Adding %s=%s to cart from MultipleProductForm', key, value)
                details = []
                formdata = request.POST
                satchmo_cart_details_query.send(
                    cart,
                    product=product,
                    quantity=quantity,
                    details=details,
                    request=request,
                    form=formdata
                )
                added_item = cart.add_item(product, number_added=quantity, details=details)
                satchmo_cart_add_complete.send(cart, cart=cart, cartitem=added_item,
                    product=product, request=request, form=formdata)
