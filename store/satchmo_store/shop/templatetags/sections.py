# -*- coding: utf-8 -*-
from django.core.cache import cache
from django.contrib.sites.models import Site
from django.template import Library, Node, Variable
from django.template import TemplateSyntaxError, VariableDoesNotExist
from pressroom.models import Section
from satchmo_utils.templatetags import get_filter_args
from django.utils.translation import get_language

register = Library()

@register.simple_tag
def list_sections():
    """ It's ugly and it should be changed ASAP"""
    sections =  Section.objects.all().order_by('id')
    output = "<ul>"
    for section in sections:
        output += "<li><a href='/news/section/"+section.slug+"'>"+section.title+"</a></li>"
    output += "</ul>"
    return output
