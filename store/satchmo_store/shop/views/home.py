# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, EmptyPage
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext as _
from livesettings import config_value
from product.views import display_featured
from pressroom.models import Article, Section
from satchmo_utils.views import bad_or_missing
from places.models import Store

def home(request, template="shop/index.html"):
    # Display the category, its child categories, and its products.
    if request.method == "GET":
        currpage = request.GET.get('page', 1)
    else:
        currpage = 1

    featured = display_featured(num_to_display=4, random_display=True)
    count = config_value('PRODUCT','NUM_PAGINATED')
    paginator = Paginator(featured, count)
    random_buy_stores = Store.featured.filter(can_buy=True).order_by('?')[:1]
    random_smoke_stores = Store.featured.filter(can_smoke=True).order_by('?')[:1]
    is_paged = False
    page = None

    try:
        paginator.validate_number(currpage)
    except EmptyPage:
        return bad_or_missing(request, _("Invalid page number"))

    is_paged = paginator.num_pages > 1
    page = paginator.page(currpage)
    articles = Article.objects.get_published().filter(sections__id=1).order_by('-pub_date')[0:3]
    promotions = Article.objects.get_published().filter(sections__id=6)[0:3]

    ctx = RequestContext(request, {
        'all_products_list' : page.object_list,
        'is_paginated' : is_paged,
        'page_obj' : page,
        'paginator' : paginator,
        'articles': articles,
        'promotions': promotions,
        'random_buy_stores': random_buy_stores,
        'random_smoke_stores': random_smoke_stores,
    })

    return render_to_response(template, context_instance=ctx)
