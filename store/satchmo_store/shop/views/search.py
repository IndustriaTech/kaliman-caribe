from django.shortcuts import render_to_response
from django.template import RequestContext
from product.models import Product
from satchmo_store.shop.forms import AdvancedSearchForm
from satchmo_store.shop import signals
from signals_ahoy.signals import application_search

def search_view(request, template="shop/search.html"):
    """Perform a search based on keywords and categories in the form submission"""
    if request.method=="GET":
        data = request.GET
    else:
        data = request.POST

    form = AdvancedSearchForm(data)
    advanced_filters = {}
    if data.get('advanced_search', None) is not None:
        if form.is_valid():
            advanced_filters = dict( (field, form.cleaned_data[field]) for field in [x.name for x in form.visible_fields()][1:] )

    keywords = data.get('keywords', '').split(' ')
    category = data.get('category', None)

    keywords = filter(None, keywords)

    results = {}

    if any(x in data for x in ["simple_search", "advanced_search"]):
        # this signal will usually call listeners.default_product_search_listener
        application_search.send(Product, request=request, 
            category=category, keywords=keywords, results=results, advanced_filters=advanced_filters)

    context = RequestContext(request, {
            'form': form,
            'advanced_filters': advanced_filters,
            'results': results,
            'category' : category,
            'keywords' : keywords,
            })
    return render_to_response(template, context_instance=context)
