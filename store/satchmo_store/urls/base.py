"""Base urls used by Satchmo.

Split out from urls.py to allow much easier overriding and integration with larger apps.
"""
from django.conf.urls.defaults import patterns, include, url
from signals_ahoy.signals import collect_urls
from product.urls.base import adminpatterns as prodpatterns
from shipping.urls import adminpatterns as shippatterns
import logging
import satchmo_store

log = logging.getLogger('shop.urls')

urlpatterns = patterns('',
    (r'^tinymce/', include('tinymce.urls')),
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    (r'^accounts/', include('satchmo_store.accounts.urls')),
    (r'^settings/', include('livesettings.urls')),
    (r'^cache/', include('keyedcache.urls')),
    (r'^news/', include('pressroom.urls')),
    (r'^photos/', include('photologue.urls')),
    (r'^shops$', 'django.views.generic.simple.direct_to_template', {'template': 'shops.html'}),
    (r'^shops-new', 'django.views.generic.simple.direct_to_template', {'template': 'shops-new.html'}),
    url(r'^stores/(?P<activity>\w*)/*(?P<city_id>\d*)', 'places.views.index', name='store_places'),
    url(r'^stores/', 'places.views.index', name='store_index'),

) + prodpatterns + shippatterns

collect_urls.send(sender=satchmo_store, patterns=urlpatterns)
