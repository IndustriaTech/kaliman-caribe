import os

from django.template.loader import add_to_builtins

from satchmo_utils.thumbnail import normalize_path
here = lambda *x: os.path.join(os.getcwd(), *x)

DEBUG = False
TEMPLATE_DEBUG = DEBUG
DIRNAME = os.path.dirname(__file__)

DJANGO_PROJECT = 'store'
DJANGO_SETTINGS_MODULE = 'store.settings'
TIME_ZONE = 'Europe/Sofia'
LANGUAGE_CODE = 'bg'
PREFIX_DEFAULT_LOCALE = True
LOCALE_REDIRECT_PERMANENT = True
LOCALE_INDEPENDENT_PATHS = (
    r'^/robots.txt',
)
SITE_ID = 1

MEDIA_ROOT = normalize_path(os.path.join(DIRNAME, 'static/'))
STATIC_ROOT = normalize_path(os.path.join(DIRNAME, 'staticfiles/'))

MEDIA_URL="/static/"
STATIC_URL="/staticfiles/"
ADMIN_MEDIA_PREFIX = '/media/'
SECRET_KEY = 'l4hb3n@dl7fisg8nj%u0zx&$g=q^f4g2-d2kv7wlfd$%u5hdv6'
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = [
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.middleware.doc.XViewMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "threaded_multihost.middleware.ThreadLocalMiddleware",
    "satchmo_store.shop.SSLMiddleware.SSLRedirect",
    #"satchmo_ext.recentlist.middleware.RecentProductMiddleware",
    #'debug_toolbar.middleware.DebugToolbarMiddleware',
    "middlewares.adult.AdultsOnlyMiddleware",
    "localeurl.middleware.LocaleURLMiddleware",
    "middlewares.flatpages.FlatpageFallbackMiddleware",
    # "debug_toolbar.middleware.DebugToolbarMiddleware",
 ]

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.i18n',
    'satchmo_store.shop.context_processors.settings',
    'django.contrib.auth.context_processors.auth',
)

ROOT_URLCONF = 'store.urls'
LOCALE_PATHS = (here('locale'),)
TEMPLATE_DIRS = (
    os.path.join(DIRNAME,'templates'),
)

INSTALLED_APPS = (
    'django.contrib.sites',
    'satchmo_store.shop',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.comments',
    'django.contrib.sessions',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',
    'django_extensions',
    'flatpages_tinymce',
    'tagging',
    'south',
    'registration',
    'sorl.thumbnail',
    'keyedcache',
    'livesettings',
    'l10n',
    'localeurl',
    'satchmo_utils.thumbnail',
    'satchmo_store.contact',
    'tax',
    'tax.modules.no',
    'tax.modules.area',
    'tax.modules.percent',
    'shipping',
    'satchmo_ext.newsletter',
    'product',
    'product.modules.configurable',
    'product.modules.custom',
    'payment',
    'payment.modules.cod',
    'satchmo_utils',
    'app_plugins',
    'tinymce',
    'photologue',
    'pressroom',
    'places',
    'hvad',
    'nani',
 )

AUTHENTICATION_BACKENDS = (
    'satchmo_store.accounts.email-auth.EmailBackend',
    'django.contrib.auth.backends.ModelBackend',
)

SATCHMO_SETTINGS = {
    'SHOP_BASE' : '',
    'MULTISHOP' : False,
    'CUSTOM_SHIPPING_MODULES' : [
        'shipping.modules.speedy_country',
        'shipping.modules.speedy_country_heavy',
        'shipping.modules.speedy_sofia_24',
        'shipping.modules.speedy_sofia_6',
        'shipping.modules.speedy_sofia_3',
        'shipping.modules.speedy_sofia_1_5',
    ]
}

SKIP_SOUTH_TESTS=True
TINYMCE_DEFAULT_CONFIG = {
        'theme': "advanced",
        'skin' : "o2k7" ,
        'skin_variant' : "silver",
        'relative_urls': False,
        'width': 650,
        'height': 450,

        'plugins' : "autolink,lists,spellchecker,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras",

        'theme_advanced_buttons1' : "save,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,|,undo,redo,|,link,unlink,anchor,image,cleanup,code",
        'theme_advanced_buttons2' : "cut,copy,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,sub,sup,|,hr,removeformat,|,insertdate,inserttime,|,preview,fullscreen",
        'theme_advanced_buttons3' : '',

        'theme_advanced_toolbar_location' : "top",
        'theme_advanced_toolbar_align' : "left",
        'theme_advanced_statusbar_location' : "bottom",
        'theme_advanced_resizing' : True,

        'content_css' : '/media/css/styles.css',
        }

INTERNAL_IPS = ('127.0.0.1', '93.152.176.57')
DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False
}
FLATPAGES_TEMPLATE_DIR =  normalize_path(os.path.join(DIRNAME, 'templates/flatpages/'))

add_to_builtins('django.templatetags.i18n')
from local_settings import *
