# coding: utf-8
from django.utils.translation import ugettext_lazy as _
from livesettings import *

SHIP_MODULES = config_get('SHIPPING', 'MODULES')
SHIP_MODULES.add_choice(('shipping.modules.speedy_country', _(u'Speedy[Цялата страна]')))
SHIPPING_GROUP = config_get_group('SHIPPING')

config_register_list(

    DecimalValue(SHIPPING_GROUP,
        'SPEEDY_COUNTRY_RATE',
        description=_(u"Доставка от един град до друг"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_country',
        default="5.00"),

    StringValue(SHIPPING_GROUP,
        'SPEEDY_COUNTRY_SERVICE',
        description=_(u"Доставка от един град до друг"),
        help_text=_(u"Доставка за цялата страна в рамките на един ден"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_country',
        default=u"Speedy за цялата страна"),

    StringValue(SHIPPING_GROUP,
        'SPEEDY_COUNTRY_DAYS',
        description=_(u"Срок на доставка"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_country',
        default=u"1 ден")
)
