# coding: utf-8
from django.utils.translation import ugettext_lazy as _
from livesettings import *

SHIP_MODULES = config_get('SHIPPING', 'MODULES')
SHIP_MODULES.add_choice(('shipping.modules.speedy_country_heavy', _(u'Speedy[Цялата страна над 3кг.]')))
SHIPPING_GROUP = config_get_group('SHIPPING')

config_register_list(

    DecimalValue(SHIPPING_GROUP,
        'SPEEDY_COUNTRY_HEAVY_RATE',
        description=_(u"Доставка от един град до друг за пратка над 3кг."),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_country_heavy',
        default="5.65"),

    StringValue(SHIPPING_GROUP,
        'SPEEDY_COUNTRY_HEAVY_SERVICE',
        description=_(u"Доставка от един град до друг, над 3кг."),
        help_text=_(u"Доставка от един град до друг в рамките на един ден"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_country_heavy',
        default=u"Speedy за цялата страна"),

    StringValue(SHIPPING_GROUP,
        'SPEEDY_COUNTRY_HEAVY_DAYS',
        description=_(u"Срок на доставка"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_country_heavy',
        default=u"1 ден")
)
