# coding: utf-8
from django.utils.translation import ugettext_lazy as _
from livesettings import *

SHIP_MODULES = config_get('SHIPPING', 'MODULES')
SHIP_MODULES.add_choice(('shipping.modules.speedy_sofia_1_5', _(u"Speedy[90 минути]")))
SHIPPING_GROUP = config_get_group('SHIPPING')

config_register_list(

    DecimalValue(SHIPPING_GROUP,
        'SPEEDY_SOFIA_1_5_RATE',
        description=_(u"Доставка в рамките на един град [90 минути]"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_sofia_1_5',
        default="14.00"),

    StringValue(SHIPPING_GROUP,
        'SPEEDY_SOFIA_1_5_SERVICE',
        description=_(u"Доставка в рамките на един град [90 минути]"),
        help_text=_("Shipping service used with Flat rate shipping"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_sofia_1_5',
        default=u"Speedy"),

    StringValue(SHIPPING_GROUP,
        'SPEEDY_SOFIA_1_5_DAYS',
        description=_("Flat Delivery Days"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_sofia_1_5',
        default=u"90 минути")
)
