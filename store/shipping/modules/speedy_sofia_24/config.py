# coding: utf-8
from django.utils.translation import ugettext_lazy as _
from livesettings import *

SHIP_MODULES = config_get('SHIPPING', 'MODULES')
SHIP_MODULES.add_choice(('shipping.modules.speedy_sofia_24', _(u"Speedy[1 ден]")))
SHIPPING_GROUP = config_get_group('SHIPPING')

config_register_list(

    DecimalValue(SHIPPING_GROUP,
        'SPEEDY_SOFIA_24_RATE',
        description=_(u"Доставка в рамките на един град [1 ден]"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_sofia_24',
        default="4.62"),

    StringValue(SHIPPING_GROUP,
        'SPEEDY_SOFIA_24_SERVICE',
        description=_(u"Доставка в София [1 ден]"),
        help_text=_("Shipping service used with Flat rate shipping"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_sofia_24',
        default=u"Speedy"),

    StringValue(SHIPPING_GROUP,
        'SPEEDY_SOFIA_24_DAYS',
        description=_("Flat Delivery Days"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_sofia_24',
        default=u"1 ден")
)
