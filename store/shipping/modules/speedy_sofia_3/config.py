# coding: utf-8
from django.utils.translation import ugettext_lazy as _
from livesettings import *

SHIP_MODULES = config_get('SHIPPING', 'MODULES')
SHIP_MODULES.add_choice(('shipping.modules.speedy_sofia_3', _(u"Speedy[3 часа]")))
SHIPPING_GROUP = config_get_group('SHIPPING')

config_register_list(

    DecimalValue(SHIPPING_GROUP,
        'SPEEDY_SOFIA_3_RATE',
        description=_(u"Доставка в рамките на един град [3 часа]"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_sofia_3',
        default="07.95"),

    StringValue(SHIPPING_GROUP,
        'SPEEDY_SOFIA_3_SERVICE',
        description=_(u"Доставка в София [3 часа]"),
        help_text=_("Shipping service used with Flat rate shipping"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_sofia_3',
        default=u"Speedy"),

    StringValue(SHIPPING_GROUP,
        'SPEEDY_SOFIA_3_DAYS',
        description=_(u"Срок на доставка"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_sofia_3',
        default=u"3 часа")
)
