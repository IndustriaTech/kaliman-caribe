# coding: utf-8
from django.utils.translation import ugettext_lazy as _
from livesettings import *

SHIP_MODULES = config_get('SHIPPING', 'MODULES')
SHIP_MODULES.add_choice(('shipping.modules.speedy_sofia_6', _(u"Speedy[6 часа]")))
SHIPPING_GROUP = config_get_group('SHIPPING')

config_register_list(

    DecimalValue(SHIPPING_GROUP,
        'SPEEDY_SOFIA_6_RATE',
        description=_(u"Доставка в рамките на един град [6 часа]"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_sofia_6',
        default="06.10"),

    StringValue(SHIPPING_GROUP,
        'SPEEDY_SOFIA_6_SERVICE',
        description=_(u"Доставка в София [6 часа]"),
        help_text=_("Shipping service used with Flat rate shipping"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_sofia_6',
        default=u"Speedy"),

    StringValue(SHIPPING_GROUP,
        'SPEEDY_SOFIA_6_DAYS',
        description=_(u"Срок на доставка"),
        requires=SHIP_MODULES,
        requiresvalue='shipping.modules.speedy_sofia_6',
        default=u"6 часа")
)
