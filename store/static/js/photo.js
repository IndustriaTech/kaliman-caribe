jQuery(document).ready(function() {
	jQuery(".review_gallery img").hide();
	jQuery(".review_gallery img:nth-child(1)").show();
	jQuery(".gallery_content li:nth-child(1) a").addClass("current_page");
	jQuery(".gallery_content li a").click(function() {
		jQuery(".gallery_content .current_page").removeClass("current_page");
		jQuery(this).addClass("current_page");
	});
	
	jQuery(".gallery_content li:nth-child(1)").click(function() {
		jQuery(".review_gallery .bigimage").hide();
		jQuery(".review_gallery img:nth-child(1)").fadeIn("300");
		return false;	
	});
	jQuery(".gallery_content li:nth-child(2)").click(function() {
		jQuery(".review_gallery .bigimage").hide();
		jQuery(".review_gallery img:nth-child(2)").fadeIn("300");
		return false;
	});
	jQuery("#screen_3").click(function() {
		jQuery(".review_gallery .bigimage").hide();
		jQuery(".review_gallery img:nth-child(3)").fadeIn("300");
		return false;	
	});
	jQuery("#screen_4").click(function() {
		jQuery(".review_gallery .bigimage").hide();
		jQuery(".review_gallery img:nth-child(4)").fadeIn("300");
		return false;
	});
	jQuery("#screen_5").click(function() {
		jQuery(".review_gallery .bigimage").hide();
		jQuery(".review_gallery img:nth-child(5)").fadeIn("300");
		return false;
	});
});
