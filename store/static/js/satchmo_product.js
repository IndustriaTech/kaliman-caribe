var satchmo = satchmo || {};

satchmo.use_sale_prices = true;

// Get the current selected product price.
satchmo.get_current_price = function(detail, qty, use_sale) {
    var taxed = satchmo.default_view_tax,
        k, prices;

    if (use_sale) {
        k = taxed ? "TAXED_SALE" : "SALE";
    }
    else {
        k = taxed ? "TAXED" : "PRICE";
    }

    var prices = detail[k];
    if (prices) {
        var best = prices['1'];
        if (qty > 1) {
            for (var pricekey in prices) {
                var priceqty = parseInt(pricekey);
                if (priceqty > qty) {
                    break;
                }
                best = prices[pricekey];
            }
        }
        return best;
    }
    else {
        return ""
    }
};

satchmo.make_optionkey = function() {
    var work = Array(satchmo.option_ids.length);
    for (var ix=0; ix<satchmo.option_ids.length; ix++) {
        var k = "#" + satchmo.option_ids[ix];
        var v = $(k).fieldValue()[0];
        work[ix] = v;
    }
    return work.join('::');
};

// used for sorting numerically
satchmo.numeric_compare = function(a, b) {
    return a-b;
};

satchmo.option_ids = "";

// Update the product name
satchmo.set_name = function(name) {
    $("#productname").attr('value', name);
};

satchmo.set_option_ids = function(arr) {
    arr.sort(satchmo.numeric_compare);
    satchmo.option_ids = arr;
};

// Update the product price
satchmo.set_price = function(price) {
    $("#price").text(price);
};

satchmo.show_error = function(msg) {
    var section = $('#js_error');
    if (section.length == 0) {
        if (msg != "") {
            $('form#options').before("<div id='js_error' class='error'>" + msg + "</div>");
        }
    }
    else {
        section.text(msg);
        if (msg == "") {
            section.hide();
        } else {
            section.show();
        }
    }
    var disabled = (msg != "");
    $('#addcart').attr('disabled', disabled);
};

// update name and price based on the current selections
satchmo.update_price = function() {
    var key = satchmo.make_optionkey(),
        detail = satchmo.variations[key],
        msg = "",
        use_sale, sale_price, full_price;
        product_name = jQuery('#product_name').html().replace('<br>', '');

    if (detail) {

        // look for images
        if('ADDITIONAL_IMAGES' in detail) {
            var i = 0;
            var images_to_stay = "<ul>";
            var image_links = "";
            jQuery('#product_images').empty();
            for (var imgx=0; imgx<detail['ADDITIONAL_IMAGES'].length; imgx++) {
                var hide_this = (imgx>0) ? ' style="display: none" ' : '';
                images_to_stay = images_to_stay + '<li' + hide_this + '><a class="gallery product_image" href="/static/' + detail['ADDITIONAL_IMAGES'][imgx] + '" title="'+product_name+'"><img src="' + satchmo.thumbnails[detail['ADDITIONAL_IMAGES'][imgx]] + '" alt="'+product_name+'" /></a></li>';
                // image_links = image_links + '<a clas="product_image" href="/static/' + detail['ADDITIONAL_IMAGES'][imgx] + '" title="'+product_name+'">'+i+'</a> ';
                i++
            }

            images_to_stay += "</ul>"

            jQuery('#product_images').html(images_to_stay);
            if(i > 1)
                jQuery('#image_links').html(i+' снимки').show();
            window.generateGallery();
        }


        var qty = parseInt($('#quantity').fieldValue()[0]);
        satchmo.set_name(detail['SLUG']);

        if (!satchmo.variations['SALE']) {
            use_sale = false;
        }
        else {
            use_sale = satchmo.use_sale_prices;
        }

        if (use_sale) {
            full_price = satchmo.get_current_price(detail, qty, false);
            $('#fullprice').text(full_price);

            sale_price = satchmo.get_current_price(detail, qty, true);
        }
        else {
            sale_price = satchmo.get_current_price(detail, qty, false);
            $('#fullprice').hide();
        }

        satchmo.set_price(sale_price);

        if (qty && qty > detail['QTY']) {
            if (detail['QTY'] <= 0) {
                msg = "Sorry, we are out of stock on that combination.";
            }
            else {
                msg = "Sorry, we only have " + detail['QTY'] + " available in that combination.";
            }
        }
    }
    satchmo.show_error(msg);
};
