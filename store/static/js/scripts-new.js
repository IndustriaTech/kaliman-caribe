﻿$(document).ready(function () 	{	
		$('.menu ul:has(li.sub) > a').addClass('more');
		$('.menu ul').hover(function () {
			$(this).find('li.sub').stop(true, true).animate({opacity: 'toggle', height: 'toggle'}, 200).addClass('active_list');
		}, function () {
			$(this).children('li.sub.active_list').stop(true, true).animate({opacity: 'toggle', height: 'toggle'}, 200).removeClass('active_list');
		});
		$("a.fancybox").fancybox({
				'overlayShow'	: false,
				'transitionIn'	: 'elastic',
				'transitionOut'	: 'elastic'
			});		
	});