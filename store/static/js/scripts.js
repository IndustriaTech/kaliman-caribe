﻿$(document).ready(function ()   {
        $(document).pngFix();
        makeSlides();
        $('#slide-items')
            .after('<div id="nav-inner" class="nav-inner">')
            .cycle({
                fx:     'fade',
                speed:  'slow',
                timeout: 5000,
                pager:  '#nav-inner'
        });
        $('#item-pics')
            .after('<div id="nav-pic" class="nav-inner">')
            .cycle({
                fx:     'fade',
                speed:  'fast',
                timeout: 5000,
                pager:  '#nav-pic'
        });
        $("#feedback").validationEngine('attach');
        $('.menu li:has(ul) > a').addClass('more');
        $('.menu li').hover(function () {
            $(this).find('ul:first').stop(true, true).animate({opacity: 'toggle', height: 'toggle'}, 200).addClass('active_list');
        }, function () {
            $(this).children('ul.active_list').stop(true, true).animate({opacity: 'toggle', height: 'toggle'}, 200).removeClass('active_list');
        });

        $("a.fancybox").fancybox({
                'overlayShow'   : true,
                'overlayColor' : '#000',
                'transitionIn'  : 'elastic',
                'transitionOut' : 'elastic'
            });
    });
function makeSlides(){
            var rotator = $('div#slider, div#product_images');


            $('.slide-items img:first', rotator).show();
            $('.nav ul.nav-inner li:first a', rotator).addClass('active');

            $('.nav ul.nav-inner li a', rotator).click(function(){
                $('.nav ul.nav-inner li a', rotator).removeClass('active');
                $(this).addClass('active');
                var link = $(this).attr('href');

                $('.slide-items img:visible', rotator).animate({
                    opacity: 0.5
                }, 500, function(){
                    $(this).removeAttr('style');

                    $(link).css({'display': 'block', 'opacity': 0.5}).animate({
                        opacity: 1
                    }, 500);
                });

                return false;
            });
        }
