from django.conf.urls.defaults import *

from satchmo_store.urls import urlpatterns
from django.views.generic.simple import direct_to_template
# from newsletter.urls import urlpatterns
urlpatterns += patterns('',
        url('^confirm_your_age', direct_to_template, {'template': 'adult_page.html'}, name="confirm_your_age"),
)
