import sys
import os

sys.path.insert(0,os.path.join(os.path.dirname(__file__), "."))
cwd = os.path.dirname(os.path.abspath(__file__))
sys.path.append(cwd + '/..')
sys.path.append(cwd)
os.environ['DJANGO_SETTINGS_MODULE'] = cwd.split("/")[-1]+'.settings'

import django.core.handlers.wsgi

application = django.core.handlers.wsgi.WSGIHandler()

